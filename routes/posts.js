const express = require('express');
const router = express.Router();

const auth = require('../middleware/authentication');
const Post = require('../models/post');

// Get all posts
router.get('/', async function (req, res, next) {
    try {
        res.json(await Post.find().populate("details.creator").populate("tags"));
    } catch (err) {
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

// Get all cover posts
router.get('/covers', async function (req, res, next) {
    try {
        res.json(await Post.find().getPostCovers().populate("details.creator").populate("tags"));
    } catch (err) {
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

// Get all post with tag
router.get('/byTag/:tagId', async function (req, res, next) {
    try {
        res.json(await Post.find({tags: req.params.tagId}).populate("details.creator").populate("tags"));
    } catch (err) {
        res.status(500).json({
            title: 'An error occured',
            error: err
        });
    }
});

// Get all covers with tag
router.get('/coversByTag/:tagId', async function (req, res, next) {
    try {
        res.json(await Post.find().getPostCoversByTag(req.params.tagId).populate("details.creator").populate("tags"));
    } catch (err) {
        res.status(500).json({
            title: 'An error occured',
            error: err
        });
    }
});

// Get one post
router.get('/:postId', async function (req, res, next) {
    try {
        let post = await Post.findById(req.params.postId).populate("details.creator").populate("tags");
        res.json(post);
    } catch (err) {
        res.status(500).json({
            title: 'An error occured',
            error: err
        });
    }
});

// Get feature picture
router.get('/:postId/featurepicture', async function (req, res, next) {
    try {
        let featurePicture = await Post.findOne().getFeaturePicture(req.params.postId);
        res.json(featurePicture);
    } catch (err) {
        res.status(500).json({
            title: 'An error occured',
            error: err
        });
    }
});

// Add post
router.post('/', auth.admin, async function (req, res, next) {
    var post = new Post(req.body);
    post.details.creationDate = new Date();

    try {
        await post.save();
        res.status(201).json(post);
    } catch (err) {
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

// Update post
router.put('/:postId', auth.admin, async function (req, res, next) {
    try {
        delete req.body._id;
		let post = await Post.findByIdAndUpdate(req.params.postId, req.body, null);
		res.json(post);
    } catch (err) {
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

// Delete post
router.delete('/:postId', auth.admin, async function(req, res, next){
    try{
        let post = await Post.findByIdAndRemove(req.params.postId);
        res.json(post);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

module.exports = router;