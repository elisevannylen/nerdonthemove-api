const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');

const auth = require('../middleware/authentication');
const User = require('../models/user');
const Config = require('../config/auth');

// Get all users
router.get('/', auth.admin, async function (req, res, next) {
    try{
        res.json(await User.find());
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

// Get one user
router.get('/:id', auth.user, async function (req, res, next) {
    try{
        let user = await User.findById(req.params.id);
        res.json(user);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

// Add user
router.post('/', auth.admin, async function (req, res, next) {
    const user = new User({
        username: req.body.username,
        email: req.body.email,
        password: await bcrypt.hash(req.body.password, Config.salt),
        fullName: req.body.fullName,
        isAdmin: req.body.isAdmin
    });

    try{
        await user.save();
        res.json({message: "successfully saved"});
    }catch(err){
        return res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

// Update user
router.put("/:userId", auth.admin, async function(req,res){
	try{
        delete req.body._id;
        if (req.body.password != null){
            req.body.password = await bcrypt.hash(req.body.password, Config.salt);
        } else {
            req.body.password = req.user.password;
        }
		let user = await User.findByIdAndUpdate(req.params.userId, req.body, null);
		res.json(user);
	}catch(err){
		res.status(500).json({
            title: 'An error occurred',
            error: err
        });
	}
});

// Delete user
router.delete('/:userId', auth.admin, async function(req, res, next){
    try{
        let user = await User.findByIdAndRemove(req.params.userId);
        res.json(user);
    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

module.exports = router;