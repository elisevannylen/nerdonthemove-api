const express = require("express");
const jwt = require("jsonwebtoken");
const authConfig = require("../config/auth");
const authM = require("../middleware/authentication");
const bcrypt = require('bcrypt');

const User = require("../models/user");

const router = express.Router();

// Login method
router.post('/login', async function (req, res, next) {
    try{
        let user = await User.findOne({email: req.body.email});

        if (user == null){
            return res.status(401).json({
                title: 'Login failed',
                error: {message: 'Invalid login credentials'}
            });
        }

        if(!await bcrypt.compare(req.body.password, user.password)){
            return res.status(401).json({
                title: 'Login failed',
                error: {message: 'Invalid login credentials'}
            });
        }

        const token = jwt.sign({userId: user._id}, authConfig.secret, {algorithm:'HS512', expiresIn:14400});
        res.json({
            message: 'Successfully logged in',
            token: token,
            userId: user._id
        })

    }catch(err){
        res.status(500).json({
            title: 'An error occurred',
            error: err
        });
    }
});

module.exports = router;