const express = require("express");
const bodyParser = require("body-parser");
const http = require("http");
const mongoose = require('mongoose');
const cors = require("cors");

const User = require("./models/user");
const auth = require('./config/auth');
const config = require('./config/config.json');
const bcrypt = require('bcrypt');

const postRoutes = require('./routes/posts');
const tagRoutes = require('./routes/tags');
const userRoutes = require('./routes/users');
const authRoutes = require('./routes/auth');

async function init(){
    const app = express();
    mongoose.Promise = global.Promise;
    await mongoose.connect('mongodb://localhost:27017/notm', {useNewUrlParser:true, useUnifiedTopology: true})

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));

    app.use(cors());
    app.use("/posts", postRoutes);
    app.use("/tags", tagRoutes);
    app.use("/users", userRoutes);
    app.use("/auth", authRoutes);

    /*app.use("/", express.static("public"));

    app.use("/*", function(req, res){
        res.sendFile(path.join(__dirname, "public/index.html"))
    });*/

    const httpServer = http.createServer(app);
    httpServer.listen(config.port, function(){
        console.log("server is running");
    });

    let countUsers = await User.countDocuments();
    if(countUsers == 0){
        let user = new User();
        user.username = "EliseVN";
        user.email = "elisevannylen@gmail.com";
        user.password = await bcrypt.hash("admin123", auth.salt);
        user.fullName = "Elise Van Nylen";
        user.isAdmin = true;
        await user.save();
    }
}

init();