const jwt = require('jsonwebtoken');
const authConfig = require("../config/auth");
const User = require("../models/user");

// Admin access
module.exports.admin = async function(req,res,next){
    try {
        let decoded = jwt.verify(req.headers.authorization, authConfig.secret);
        if (decoded == null){
            throw new Error("decoding error");
        }

        req.user = await User.findById(decoded.userId);
        if (req.user == null){
            throw new Error("user doesn't exist");
        }

        if (!req.user.isAdmin){
            throw new Error("user has no rights");
        }

        next();
    } catch (err) {
        res.status(401).json({
            message: err.message
        })
    }
};

// User access
module.exports.user = async function(req,res,next){
    try {
        let decoded = jwt.verify(req.headers.authorization, authConfig.secret);
        if (decoded == null){
            throw new Error("decoding error");
        }

        req.user = await User.findById(decoded.userId);
        if (req.user == null){
            throw new Error("user doesn't exist");
        }

        next();
    } catch (err) {
        res.status(401).json({
            message: err.message
        })
    }
};