# Nerdonthemove API

**NOTM (Nerd on the Move)** is a blog about life as a former exchange student, full-time volunteer and working IT-girl. 

This API serves the blog and gives the right information from a **MongoDB** database.
API written in **Node.js**.