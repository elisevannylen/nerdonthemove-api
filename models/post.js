const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const idValidator = require('mongoose-id-validator');

const pictureSchema = new Schema({
    description: {type: String, required: true},
    location: {type: String, required: true},
    changeDate: {type:Schema.Types.Date, required: true},
    isFeaturePicture: {type: Boolean, required: true}
});

const postcoverSchema = new Schema({
    title: {type: String, required: true},
    description: {type: String, required: true},
    writeDate: {type:Schema.Types.Date, required: false},
    creationDate: {type:Schema.Types.Date, required: true},
    creator: {type: Schema.Types.ObjectId, ref:'User', required: true},
    hasFeaturePicture: {type: Boolean, required: true},
    isActive:{type: Boolean, required: true}
});

const postSchema = new Schema({
    details: postcoverSchema,
    text: {type: String, required: true},
    pictures: [pictureSchema],
    tags: [{type:Schema.Types.ObjectId, ref:'Tag', default:[]}]
});

postSchema.plugin(idValidator);

pictureSchema.pre('validate', function(next){
    this.changeDate = new Date();
    next();
});

postcoverSchema.pre('validate', function(next){
    if(this.isActive == undefined || this.isActive == null){
        this.isActive = false;
    }
    next();
});

postSchema.query.getPostCovers = function() {
    return this.select({details: 1, tags: 1});
};

postSchema.query.getPostCoversByTag = function(tagId) {
    return this.where({tags: tagId}).select({details: 1, tags: 1});
};

postSchema.query.getPostCoversByUser = function(userId) {
    return this.where({details: {creator: userId}}).select({details: 1, tags: 1});
};

postSchema.query.getFeaturePicture = function(postId) {
    return this.where({_id: postId}).select({pictures:{$elemMatch:{isFeaturePicture: true}}});
};

module.exports = mongoose.model('Post', postSchema);