const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongooseUniqueValidator = require('mongoose-unique-validator');

const userSchema = new Schema({
    username: {type:String, required:true},
    email: {type:String, required:true, unique:true},
    password: {type:String, required:true},
    fullName: {type:String, required:true},
    isAdmin: {type:Boolean, required:true}
});

userSchema.plugin(mongooseUniqueValidator);

userSchema.pre('validate', function(next){
    if(this.isAdmin == null){
        this.isAdmin = false;
    }
    next();
});

module.exports = mongoose.model('User', userSchema);